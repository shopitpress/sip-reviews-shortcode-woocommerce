<?php

	$src_image = SIP_RSWC_URL . 'admin/assets/images/';
?>
<div class="wrap">

<div class="roww">

<div class="col-md-12 col-xl-12 pd-0">
<div class="alert alert-primary d-flex align-items-center">
 <p class="mg-b-0"> Upgrade to <span class="hl">PRO version</span> to benefit from all these awesome features! </p>
 
<a class="sip-link" href="<?php echo SIP_RSWC_PLUGIN_URL . '?utm_source=wordpress.org&utm_medium=SIP-panel&utm_content=v'. SIP_RSWC_VERSION .'&utm_campaign=' .SIP_RSWC_UTM_CAMPAIGN; ?>" target="_blank" class="mg-l-auto btn btn-danger">UPGRADE</a>
</div>
</div>
<div class="row">
  <div class="col-md-6 col-xl-3">
  <div class="card ht-100p">
  <img class="card-img-top" src=<?php echo $src_image . 'styles-new.png';?> alt="Multiple styles" />
  <div class="card-body">
  <h5 class="card-title">2 more awesome styles</h5>
  <p>With these two beautifully crafted designs you have more options to display your product reviews in an engaging way.</p>
  </div>
</div>
  </div>
  <div class="col-md-6 col-xl-3">
 <div class="card ht-100p">
  <img class="card-img-top" src=<?php echo $src_image . 'review-new.png';?> alt="Submit a review shortcode"/>
  <div class="card-body">
  <h5 class="card-title">Submit form shortcode</h5>
  <p>If you display WooCommerce reviews in a normal post or page, perhaps you want to let customers submit their own reviews as well. With the pro version we have developed a new shortcode that will let you display the submit reviews form anywhere.</p>
  </div>
</div>
  </div>
  <div class="col-md-6 col-xl-3"> 
    <div class="card ht-100p">
  <img class="card-img-top" src=<?php echo $src_image . 'aggr-new.png';?> alt="Aggregated reviews"/>
  <div class="card-body">
  <h5 class="card-title">Aggregated reviews</h5>
   <p>Do you use several SKUs for the same product? Do you want to display reviews of a whole category or categories instead of a single product? With the pro version you can do all this easily.</p>
  </div>
</div>
  </div>
  <div class="col-md-6 col-xl-3">
  <div class="card ht-100p">
  <img class="card-img-top" src=<?php echo $src_image . 'lan-new.png';?> alt="Multi-language"/>
  <div class="card-body">
  <h5 class="card-title">Multi language support</h5>
   <p>Now you can translate the plugin to any language you want using language files, easy and fast!</p>
  </div>
</div>
  </div>
  </div>


<div class="col-md-12 col-xl-12 pd-0 mg-t-20">
<div class="alert alert-primary d-flex align-items-center">
 <p class="mg-b-0"> Upgrade to <span class="hl">PRO version</span> to benefit from all these awesome features! </p>
 
<a class="sip-link" href="<?php echo SIP_RSWC_PLUGIN_URL . '?utm_source=wordpress.org&utm_medium=SIP-panel&utm_content=v'. SIP_RSWC_VERSION .'&utm_campaign=' .SIP_RSWC_UTM_CAMPAIGN; ?>" target="_blank" class="mg-l-auto btn btn-danger">UPGRADE</a>
				
</div>
</div>

</div>

</div>


