
	
<div class="card ">
  <div class="card-header"> <h5 class="card-title"><?php _e('Settings' , 'sip-reviews-shortcode'); ?></h5></div>
 
  
 



  	<form method="post" action="options.php">
	  	<?php settings_fields( 'sip-rswc-settings-group' ); ?>
		
		
	 <div class="card-body">
       <p class="sip"><?php _e('Set a limit on the number of characters for the reviews. Setting the limit to 0 will remove the word limit from the reviews.' , 'sip-reviews-shortcode');?></p>
  <div class="form-group mg-t-20">
 
  <input class="form-control"  id="sip-rswc-setting-limit-review-characters" type="number" name="sip-rswc-setting-limit-review-characters" value="<?php echo get_option('sip-rswc-setting-limit-review-characters'); ?>"  min="0"/>

  </div>
  
   </div>
  
  <div class="card-footer"><input type="submit" name="submit" id="submit" class="btn btn-danger" value="Update"  /></div>
  
  
		<?php //submit_button(); ?>
	</form>
</div>
