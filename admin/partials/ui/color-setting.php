<?php
$options = get_option( 'color_options' );
	$star_color = ( isset( $options['star_color'] ) ) ? sanitize_text_field( $options['star_color'] ) : '';
	$bar_color = ( isset( $options['bar_color'] ) ) ? sanitize_text_field( $options['bar_color'] ) : '#AD74A2';
	$review_body_text_color = ( isset( $options['review_body_text_color'] ) ) ? sanitize_text_field( $options['review_body_text_color'] ) : '';
	$review_background_color = ( isset( $options['review_background_color'] ) ) ? sanitize_text_field( $options['review_background_color'] ) : '';
	$review_title_color = ( isset( $options['review_title_color'] ) ) ? sanitize_text_field( $options['review_title_color'] ) : '';
	$load_more_button = ( isset( $options['load_more_button'] ) ) ? sanitize_text_field( $options['load_more_button'] ) : '';
	$load_more_text  = ( isset( $options['load_more_text'] ) ) ? sanitize_text_field( $options['load_more_text'] ) : '';

	?>
	
	 <div class="form-group">
    <label for="color_options[star_color]" >Review stars</label>
   <input class="sipcolorpicker" name="color_options[star_color]" type="text" value="<?php echo $star_color ?>" />
	<div id="star-colorpicker"></div>
  </div>
  
  
		 <div class="form-group">
    <label for="color_options[bar_color]" >Reviews bar summary</label>
   <input class="sipcolorpicker" name="color_options[bar_color]" type="text" value="<?php echo $bar_color ?>" />
	<div id="bar-colorpicker"></div>
  </div>
  
		 <div class="form-group">
    <label for="color_options[review_background_color]" >Review background</label>
   	<input class="sipcolorpicker" name="color_options[review_background_color]" type="text" value="<?php echo $review_background_color ?>" />
	<div id="review-background-colorpicker"></div>
  </div>
	
		
		 <div class="form-group">
    <label for="color_options[review_body_text_color]" >Review body text</label>
   <input class="sipcolorpicker" name="color_options[review_body_text_color]" type="text" value="<?php echo $review_body_text_color ?>" />
				<div id="review-body-text-colorpicker"></div>
  </div>
	
		 <div class="form-group">
    <label for="color_options[review_title_color]" >Review title</label>
   <input class="sipcolorpicker" name="color_options[review_title_color]" type="text" value="<?php echo $review_title_color ?>" />
				<div id="review-title-colorpicker"></div>
  </div>
	
		 <div class="form-group">
    <label for="formGroupExampleInput" >Load more button background</label>
   <input class="sipcolorpicker" name="color_options[load_more_button]" type="text" value="<?php echo $load_more_button ?>" />
				<div id="load-more-button-colorpicker"></div>
  </div>
	
		
		 <div class="form-group">
    <label for="formGroupExampleInput" >Load more button text</label>
   <input class="sipcolorpicker" name="color_options[load_more_text]" type="text" value="<?php echo $load_more_text ?>" />
				<div id="load-more-button-text-colorpicker"></div>
  </div>
  	<?php settings_fields( 'wp_color_picker_options' ); ?>
			<?php do_settings_sections( 'wp-color-picker-settings' ); ?>

