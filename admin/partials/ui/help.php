<div class="row">

<div class="col-md-7 col-xl-7 mg-b-20 mg-t-10">
<div class="card ht-100p">

<div class="card-header"> <h5 class="card-title">How to use?</h5></div>
 <div class="card-body">
    <p><strong class="strong">Shortcode usage example:</strong></p>
    <code>[woocommerce_reviews id="2075" product_title="My Awesome Product" no_of_reviews="5" ]</code>
    <p>&nbsp;</p>
    <p><strong>id</strong> (optional) is the ID of the WooCoommerce product you want to display. [woocommerce_reviews] without id input will show the reviews of the current product if applicable.</p><p>
      <strong>product_title</strong> (optional) is the name of the product you want to display. This is used for Schema only (by default is the product name in WooCommerce).</p><p>
      <strong>no_of_reviews</strong> (optional) is the number of reviews loaded per page (default = 5). Additional reviews can be loaded after clicking in "Load more" button.</p><p>
    </p>
    <p>SIP Reviews Shortcode for WooCommerce is fully-compatible with Schema.org, allowing you to display product ratings directly from Google's results. You may test your Schema at <a class="sip-link" href="https://developers.google.com/structured-data/testing-tool/">Google Structured Data Testing Tool</a>.</p>
    
  </div>		
			
	</div>		
           
          

</div>
<div class="col-md-5 col-xl-5 mg-b-20 mg-t-10">
<div class="card mg-b-20">

<div class="card-header"> <h5 class="card-title">Questions and support</h5></div>
 <div class="card-body">
    <p>All of our plugins come with free support. We care about your plugin after purchase just as much as you do.</p>
    <p>We want to make your life easier and make you happy about choosing our plugins. We guarantee to respond to every inquiry within 1 business day.
    Please visit our <a class="sip-link" href="https://shopitpress.com/community/?utm_source=wordpress.org&amp;utm_medium=backend-help&amp;utm_campaign=sip-reviews-shortcode-woocommerce" target="_blank">community</a> and ask us anything you need.</p>
  </div>    
      
  </div>  
           
          

</div>

</div>

