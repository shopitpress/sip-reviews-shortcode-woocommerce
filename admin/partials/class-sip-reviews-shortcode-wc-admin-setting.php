<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://shopitpress.com
 * @since      1.0.4
 *
 * @package    SIP_Reviews_Shortcode
 * @subpackage SIP_Reviews_Shortcode/admin/partials
 */

/**
 * After loding this function global page show the admin panel
 *
 * @since    	1.0.0
 */
function sip_rswc_settings_page_ui() { ?>

<div class="tab-pane fade show active">
<div class="row">

<div class="col-md-6 col-xl-6 mg-t-10">

<div class="card ht-100p ">
  <div class="card-header"> <h5 class="card-title">Custom Color Settings</h5></div>
    <form id="wp-color-picker-options" action="options.php" method="post">
  <div class="card-body">
			
<?php include( SIP_RSWC_DIR . 'admin/partials/ui/color-setting.php'); ?>
		

			</div>
			<div class="card-footer">
			
<input id="wp-color-picker-submit" name="Submit" type="submit" class="btn btn-danger" value="<?php _e( 'Save Color' ); ?>" />
			</div>

		</form>
</div> 

</div>

<div class="col-md-6 col-xl-6 mg-t-10 het fto">
<?php include( SIP_RSWC_DIR . 'admin/partials/ui/settings.php'); ?>

</div>


<!-- card -->  
		
		</div>  
		</div>
	

	<?php
}

/**
 * Register settings, add a settings section, and add our color fields.
 *
 * @since    	1.0.0
 */
function sip_rswc_settings_init(){

	register_setting(
		'wp_color_picker_options',
		'color_options',
		'validate_options'
	);
}

function validate_options( $input ){
	$valid 								= array();
	$valid['star_color'] 				= sanitize_text_field( $input['star_color'] );
	$valid['bar_color'] 				= sanitize_text_field( $input['bar_color'] );
	$valid['review_body_text_color'] 	= sanitize_text_field( $input['review_body_text_color'] );
	$valid['review_background_color']	= sanitize_text_field( $input['review_background_color'] );
	$valid['review_title_color'] 		= sanitize_text_field( $input['review_title_color'] );
	$valid['load_more_button'] 			= sanitize_text_field( $input['load_more_button'] );
	$valid['load_more_text'] 			= sanitize_text_field( $input['load_more_text'] );

	return $valid;
}


function color_input(){
	$options = get_option( 'color_options' );
	$star_color = ( isset( $options['star_color'] ) ) ? sanitize_text_field( $options['star_color'] ) : '';
	$bar_color = ( isset( $options['bar_color'] ) ) ? sanitize_text_field( $options['bar_color'] ) : '#AD74A2';
	$review_body_text_color = ( isset( $options['review_body_text_color'] ) ) ? sanitize_text_field( $options['review_body_text_color'] ) : '';
	$review_background_color = ( isset( $options['review_background_color'] ) ) ? sanitize_text_field( $options['review_background_color'] ) : '';
	$review_title_color = ( isset( $options['review_title_color'] ) ) ? sanitize_text_field( $options['review_title_color'] ) : '';
	$load_more_button = ( isset( $options['load_more_button'] ) ) ? sanitize_text_field( $options['load_more_button'] ) : '';
	$load_more_text  = ( isset( $options['load_more_text'] ) ) ? sanitize_text_field( $options['load_more_text'] ) : '';

	?>
	
	 <div class="form-group">
    <label for="color_options[star_color]" >Review stars</label>
   <input class="sipcolorpicker" name="color_options[star_color]" type="text" value="<?php echo $star_color ?>" />
	<div id="star-colorpicker"></div>
  </div>
  
  
		 <div class="form-group">
    <label for="color_options[bar_color]" >Reviews bar summary</label>
   <input class="sipcolorpicker" name="color_options[bar_color]" type="text" value="<?php echo $bar_color ?>" />
	<div id="bar-colorpicker"></div>
  </div>
  
		 <div class="form-group">
    <label for="color_options[review_background_color]" >Review background</label>
   	<input class="sipcolorpicker" name="color_options[review_background_color]" type="text" value="<?php echo $review_background_color ?>" />
	<div id="review-background-colorpicker"></div>
  </div>
	
		
		 <div class="form-group">
    <label for="color_options[review_body_text_color]" >Review body text</label>
   <input class="sipcolorpicker" name="color_options[review_body_text_color]" type="text" value="<?php echo $review_body_text_color ?>" />
				<div id="review-body-text-colorpicker"></div>
  </div>
	
		 <div class="form-group">
    <label for="color_options[review_title_color]" >Review title</label>
   <input class="sipcolorpicker" name="color_options[review_title_color]" type="text" value="<?php echo $review_title_color ?>" />
				<div id="review-title-colorpicker"></div>
  </div>
	
		 <div class="form-group">
    <label for="formGroupExampleInput" >Load more button background</label>
   <input class="sipcolorpicker" name="color_options[load_more_button]" type="text" value="<?php echo $load_more_button ?>" />
				<div id="load-more-button-colorpicker"></div>
  </div>
	
		
		 <div class="form-group">
    <label for="formGroupExampleInput" >Load more button text</label>
   <input class="sipcolorpicker" name="color_options[load_more_text]" type="text" value="<?php echo $load_more_text ?>" />
				<div id="load-more-button-text-colorpicker"></div>
  </div>

 	<?php
}